#include "NEW_Aruco.h"

void ArucoDetector::update()
{
	_marker_found = false;
	// gcs().send_text(MAV_SEVERITY_INFO, "Any?");
	// If there's no image from stream, exit the function
	if (! _cam->_post_image_set)
	{
		return;
	}

	// gcs().send_text(MAV_SEVERITY_INFO, "Anyyy?");

	cv::Mat image = _cam->_post_image.clone();

	cv::cvtColor(image, image, cv::COLOR_RGB2BGR);
	std::vector<int> ids;
	std::vector<std::vector<cv::Point2f>> corners, rejected_corners;

	cv::aruco::detectMarkers(image, _dictionary, corners, ids, _detector_params, rejected_corners);

	// Check whether we get any marker

	if(ids.empty())
	{
		return;
	}
	// gcs().send_text(MAV_SEVERITY_INFO, "go?");

	// Get only one marker
	
	_detected_marker_id = ids.front();
	// std::cout << "hurah22?" << std::endl;
	std::vector<cv::Point2f> detected_marker_corners = corners.front();

	// Easier method: get the distance from the center of the marker

	_marker_distance_to_center = ArucoDetector::markerDistanceFromCenter(detected_marker_corners, _cam->_height, _cam->_width);

	// Estimate pose from one marker

	// std::vector<cv::Vec3d> rotation_vector, translation_vector;

	// cv::aruco::estimatePoseSingleMarkers(corners, _marker_size, _cam->_intrinsic_matrix, _cam->_distortion_coefficient, rotation_vector, translation_vector);

	// // calculate positional coordinate of the marker

	// cv::Matx33d rotation_matrix = cv::Matx<double, 3, 3>(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	// cv::Rodrigues(rotation_vector, rotation_matrix);
	// cv::Mat res = -1 * (cv::Mat)rotation_matrix;
	// cv::Mat res2;
	// cv::transpose(res, res2);
	// cv::Mat translation_vector_ = cv::Mat(translation_vector.front());
	// translation_vector_ = translation_vector_.reshape(1, 3);
	// std::cout << res2 << std::endl;
	// std::cout << translation_vector_ << std::endl;
	// res = res2 * translation_vector_;

	// cv::Vec3d coordinate(res);

	// // TODO: Get target attitude
	// // TEST: set 0,0,0 as drone's position and orientation
	// cv::Vec3d self_pos(0.0, 0.0, 0.0);
	// cv::Vec3d self_rot(0.0, 0.0, 0.0);

	// _target_position = self_pos - coordinate;

	// ArucoDetector::getEulerAngles(rotation_vector.front(), self_rot);

	// // std::cout << "heiheihei?4" << std::endl;

	// _target_orientation = self_rot;
	// _target_set = true;
	_marker_found = true;
	gcs().send_text(MAV_SEVERITY_INFO, "Got Aruco Marker");
}

void ArucoDetector::init(float marker_size)
{
	_marker_size = marker_size;
}

ArucoDetector *ArucoDetector::_singleton;